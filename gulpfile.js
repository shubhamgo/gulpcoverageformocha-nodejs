'use strict';
const gulp = require('gulp');
var gulp1        = require('gulp');
var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify');

// Mocha, our test framework
var mocha = require('gulp-mocha');

// Istanbul, our code coveranpmge tool
var istanbul = require('gulp-istanbul'); 

var cover = require('gulp-coverage');

gulp.task('test', function () {
    return gulp.src('test/**/*.js', { read: false })
            .pipe(cover.instrument({
                pattern: ['**/test*'],
                debugDirectory: 'debug'
            }))
            .pipe(mocha())
            .pipe(cover.gather())
            .pipe(cover.format())
            .pipe(gulp.dest('reports'));
});





