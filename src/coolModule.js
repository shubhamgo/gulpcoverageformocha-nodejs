var coolModule = {};

coolModule.doSum = function(a, b) {
  return a + b;
}

coolModule.doSubtraction = function(a, b) {
  return a - b;
}

module.exports = coolModule;