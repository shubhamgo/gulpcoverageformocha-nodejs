var coolModule = require('../index.js');
var assert = require('gulp-mocha').assert;

describe('Cool Module', function() {
    it('should do sums correctly', function() {
        var result = coolModule.doSum(1, 2);
        assert.strictEqual(result, 3);
    });

    it('should do subtractions correctly', function() {
        var result = coolModule.doSubtraction(2, 1);
        assert.strictEqual(result, 1);
    });
});